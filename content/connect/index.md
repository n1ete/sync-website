# Community Resources

Chat: [Mattermost](https://talk.lilbits.de/signup_user_complete/?id=m8q3fugujjb8uqx5kcutzj98wc)

Jam: [Ninjam](http://sync.lilbits.de) and [JamTaba](https://jamtaba-music-web-site.appspot.com/) via `ninjam.lilbits.de:2334`

File exchange: [Seafile](https://files.lilbits.de)

Physical meetup: [Raststätte](http://raststaette.org/)
